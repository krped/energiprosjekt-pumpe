let fullSize = false;

document.querySelectorAll("img[lightbox]").forEach(img => {
    img.addEventListener("click", () => {
        fullSize = !fullSize;
        if (fullSize) {
            img.style.width = "auto";
            img.style.height = "66vh";
            img.style.position = "absolute";
            img.style.top = "0";
            img.style.left = "0";
            img.style.maxWidth = "95vw";
        } else {
            img.style.width = "33vh";
            img.style.height = "auto";
            img.style.position = "static";
        }
    });
});
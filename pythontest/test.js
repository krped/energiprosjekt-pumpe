document.addEventListener("keypress", event => {
	console.log(event.key);
});

// Få videoene til å vises riktig
const video = document.querySelector("video");
const videoHeight = 1000;
let currentVideo = "";

video.style.height = videoHeight;
video.style.left = (window.innerWidth / 2) - (video.getBoundingClientRect().width / 2) - 5;

video.addEventListener("click", () => {
	video.paused ? video.play() : video.pause();	
})

video.addEventListener("loadeddata", () => {
	video.style.left = (window.innerWidth / 2) - (video.getBoundingClientRect().width / 2) - 5;
	video.style.display = "block";
});

// Klikk -> skift video
const buttons = document.querySelectorAll("button");

buttons.forEach(button => {
	button.addEventListener("click", changeVideo);
	
    // Fikk ikke til ønsket hover-effekt, så måtte programmere det. :(
    button.addEventListener("mouseenter", () => {
		if(button.innerHTML !== currentVideo || currentVideo === "") {
			button.style.backgroundColor = "#afffaf";
		} else {
			button.style.backgroundColor = "lime";
		}
	});
	
	button.addEventListener("mouseleave", () => {
		if(button.innerHTML !== currentVideo || currentVideo === "") {
			button.style.backgroundColor = "white";
		} else {
			button.style.backgroundColor = "lime";
		}
	});	
});

// Skift video og skift farge på videoens knapp 
function changeVideo() {
    const videoPath = "video/" + this.innerHTML + ".mp4";
    currentVideo = this.innerHTML.split(".mp4")[0];
    
    buttons.forEach(function(button) {
		if(button.innerHTML === currentVideo) {
			button.style.backgroundColor = "lime";
		} else {
			button.style.backgroundColor = "white";
		}
	});
	
    video.src = videoPath;
    video.play();
}

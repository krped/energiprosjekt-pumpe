from pyautogui import press
from time import sleep
import tkinter as tk
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

def blink():
	for i in range(3):
		sleep(0.5)
		print("----------------------")
		for j in range(4):
			output = 'Led ' + str(j+1)
			print(output)
			sleep(0.1)

def send_tastetrykk(tast):
	# Grensesnittet tar seg av håndteringen av tastetrykkene
	
	# 0: Tilbake til hovedsiden
	# 1: Elektrisitet
	# 2: Hydrogen
	# 3: Biodrivstoff

	# e: Spill el-lyd
	# h: Spill hydrogenlyd
	# b: Spill biolyd
	press(tast)

while True:
	Thread(target=send_tastetrykk("")).start()

#!/usr/bin/python3.2

#Racingstarttrainer by Ingolf Brudeli & Robin Brudeli
#2015.07.07 Copyright

import time
import tkinter as tk
import RPi.GPIO as GPIO
import random

state = False
stopString = '0.000'
timeString = '0.000'
textzero = '0.000'

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(3, GPIO.OUT)
GPIO.setup(5, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(19, GPIO.OUT)
GPIO.setup(21, GPIO.OUT)
GPIO.setup(29, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(33, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(37, GPIO.IN, pull_up_down=GPIO.PUD_UP)

root = tk.Tk()
root.title('Starttrainer')
root.geometry("1280x720")
root.configure(background='black')

def update_timeText():
    if (state):
        global timer
        global timeString
      
        timer[1] = time.time() - timer[0]
        
        if timer[1] >= 30:
            stop(1)
      
        # We create our time string here
        tempVar = (time.time() - timer[0]) * 1000
        tempVar = int(tempVar) / 1000
        timeString = "%02.3f" %tempVar
        # Update the timeText Label box with the current time
        timeText.configure(text=timeString)        
        # Call the update_timeText() function after 10 milliseconds
        root.after(10, update_timeText)
    if not state:
        global stopString
        timeText.configure(text=stopString)
    
# To start the starttrainer.
def remoteSwitch(chnl):
    start()
        
def start():
    global state
    global textzero
    if not state:
        timeText.configure(text=textzero)
        state = True
        lightflash()
        
def lightflash():
    count= 0
    while (count < 5):
        GPIO.output(11,1)
        time.sleep(0.3)
        GPIO.output(11,0)
        GPIO.output(13,1)
        time.sleep(0.3)
        GPIO.output(13,0)
        GPIO.output(19,1)
        time.sleep(0.3)
        GPIO.output(19,0)
        count = count + 1
    time.sleep(3)
    ready()

def ready():
    global timer
    GPIO.output(11,1)
    time.sleep(1)
    GPIO.output(13,1)
    time.sleep(1)
    GPIO.output(19,1)
    time.sleep(1+5*random.random())
    GPIO.output(11,0)
    GPIO.output(13,0)
    GPIO.output(19,0)
    timer = [0, 0]
    timeText.configure(text='0.000')
    timer[0] = time.time()
    update_timeText()
    
# To stop the starttrainer.
def stopBtn():
    stop(2)
    
def stop(channel):
    global state
    global timer
    global stopString
    if state:
        stopString = (time.time() - timer[0]) * 1000
        stopString = int(stopString) / 1000  
        state = False

GPIO.add_event_detect(37, GPIO.FALLING, callback=stop)
GPIO.add_event_detect(33, GPIO.FALLING, callback=remoteSwitch, bouncetime=100) 

# Create the GUI
timeText = tk.Label(root, justify = "right", text="0.000", bg="black", fg="white", font=("Arial 310 bold"))
timeText.pack(side='right')

startButton = tk.Button(root, command=start, text="Start", bg='green', fg='white', font="Arial 40 bold").place(x=200,y=550)

stopButton = tk.Button(root, command=stopBtn, text="Stop", bg='red', fg='white', font="Arial 40 bold").place(x=10, y=550)

root.mainloop()





